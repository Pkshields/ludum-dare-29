﻿using UnityEngine;
using System.Collections;

public class FatEnemyScript : MonoBehaviour {
    public float factorToIncreaseBy;
    public float factorToReduceBy;

	private void Start () {
	
	}
	
	private void FixedUpdate () {
        ChangeScale(factorToIncreaseBy);
	}

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == Tags.PLAYERBULLET) {
            ChangeScale(-factorToReduceBy);
        }
    }

    public void ChangeScale(float factor) {
        Vector3 scale = this.transform.localScale;
        scale.x += factor;
        scale.y += factor;
        this.transform.localScale = scale;
    }
}
