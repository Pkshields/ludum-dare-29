﻿using UnityEngine;
using System.Collections;
using UnityBase.Math;

public class WitchEnemyMovementScript : MonoBehaviour {
    public float idleSpeed;
    public float startledSpeed;
    public float minTimeToIdleInDirection;
    public float maxTimeToIdleInDirection;

    private GameObject player;
    private float timeBeforeNewDirection;
    private float currentTime;
    private bool isStartled;

	private void Start () {
        GameObject[] players = GameObject.FindGameObjectsWithTag(Tags.PLAYER);
        if (players.Length == 0) {
            throw new MissingComponentException("Could not find the Player object");
        }
        player = players[0];

        timeBeforeNewDirection = Random.Range(minTimeToIdleInDirection, maxTimeToIdleInDirection);
        this.rigidbody2D.velocity = GetRandomDirection() * idleSpeed;
        currentTime = 0f;
        isStartled = false;
	}
	
    private void Update () {
        if (!isStartled) {
            currentTime += Time.deltaTime * 1000;
            if (currentTime >= timeBeforeNewDirection) {
                GenerateNewDirection();
                timeBeforeNewDirection = Random.Range(minTimeToIdleInDirection, maxTimeToIdleInDirection);
                currentTime = 0f;
            }
        }
        else {
            MoveTowardsPlayerStartled();
        }
	}

    private void GenerateNewDirection() {
        timeBeforeNewDirection = Random.Range(minTimeToIdleInDirection, maxTimeToIdleInDirection);
        this.rigidbody2D.velocity = GetRandomDirection() * idleSpeed;
    }

    private Vector2 GetRandomDirection() {
        return Random.insideUnitCircle.normalized;
    }

    public void Startle() {
        isStartled = true;

        MoveTowardsPlayerStartled();
    }

    private void MoveTowardsPlayerStartled() {
        Vector2 directionVector = VectorHelper.CalculateDirectionalVector(this.transform.position, player.transform.position);
        this.rigidbody2D.velocity = directionVector * startledSpeed;
    }

    public bool IsStarted() {
        return isStartled;
    }
}
