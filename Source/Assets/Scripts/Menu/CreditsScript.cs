﻿using UnityEngine;
using System.Collections;
using UnityBase;
using UnityBase.Menu;

public class CreditsScript : MonoBehaviour
{
	public GUISkin skin;
	public GUISkin headerSkin;
	public GUISkin buttonSkin;
	public int screenWidth;
	public int screenHeight;
	public string sceneToGoBackTo;

	private Menu testMenu;

	private void Start() {
		float buttonWidth = 200f;
		float buttonHeight = 40f;

		testMenu = new Menu(true);
		testMenu.SetGUISkin(buttonSkin);

		MenuButton back = new MenuButton("back", "Back to Menu", new Rect(50, screenHeight - 50, buttonWidth, buttonHeight));
		back.buttonPressed += OnBackButton;
		testMenu.AddButton(back);
	}


	private void Update() {
		// TODO: Add controller support for the back button
	}
	
	private void OnGUI() {
		int xPadding = 50, 
			yPadding = 50;

		string credits = 
@"Code (and incredible graphics) created by Paul Shields


Credit and thanks go to:

RoccoW and geir tjelta at FreeMusicArchive.org for their music
MoikMellah at OpenGameArt.org for the man sprite on the main menu
Bfxr for instant sound effects
";

		GUIHelper.ScaleGUIToScreenSize(screenWidth, screenHeight);

		GUI.skin = headerSkin;
		GUI.Label(new Rect(screenWidth / 2, yPadding, screenWidth - xPadding, screenHeight - yPadding), "Credits");
		GUI.skin = skin;
		GUI.Label(new Rect(xPadding, yPadding + 100, screenWidth - xPadding, screenHeight - yPadding), credits);

		testMenu.DrawAndHandle();

		GUIHelper.ResetGUIScreenSize();
	}

	private void OnBackButton() {
		Application.LoadLevel(sceneToGoBackTo);
	}
}
