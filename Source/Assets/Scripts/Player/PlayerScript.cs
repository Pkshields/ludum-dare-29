﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {
    private PlayerDataScript playerData;

	private void Start () {
        playerData = FindObjectOfType<PlayerDataScript>();

        this.rigidbody2D.fixedAngle = true;
	}
	
	private void Update () {
	
	}

    private void OnTriggerEnter2D(Collider2D other) {
        HandleCollision(other);
    }
    
    private void OnTriggerStay2D(Collider2D other) {
        HandleCollision(other);
    }

    private void HandleCollision(Collider2D other) {
        if (other.tag == Tags.ENEMYBULLET) {
            EnemyBulletScript bullet = other.GetComponent<EnemyBulletScript>();
            int damageToDo = bullet.GetDamage();
            playerData.DoDamage(damageToDo);
        }
    }
}
