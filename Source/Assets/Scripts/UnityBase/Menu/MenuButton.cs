﻿using UnityEngine;
using System.Collections;

namespace UnityBase.Menu
{
	/// <summary>
	/// Menu button
	/// </summary>
	public class MenuButton
	{
		/// <summary>
		/// Unique name for this control
		/// </summary>
		/// <value>The name of the control</value>
		public string controlName
		{
			get;
			private set;
		}

		/// <summary>
		/// Text to show on the button
		/// </summary>
		/// <value>The text</value>
		public string text
		{
			get;
			private set;
		}

		/// <summary>
		/// Rect to draw the button at
		/// </summary>
		/// <value>The rect</value>
		public Rect rect
		{
			get;
			private set;
		}

		/// <summary>
		/// Event to call when the button is pressed
		/// </summary>
		public delegate void OnButtonPress();
		public event OnButtonPress buttonPressed;

		/// <summary>
		/// Initializes a new instance of the <see cref="UnityBase.Menu.MenuButton"/> class
		/// </summary>
		/// <param name="controlName">Unique control name</param>
		/// <param name="text">Text to show on the button</param>
		/// <param name="rect">Rect to draw the button at</param>
		public MenuButton(string controlName, string text, Rect rect) 
		{
			this.controlName = controlName;
			this.text = text;
			this.rect = rect;
		}

		/// <summary>
		/// Draws the button and handle mouse clicks
		/// </summary>
		public void DrawAndHandle()
		{
			GUI.SetNextControlName(controlName);
			if (GUI.Button(rect, text))
			{
				HandleButtonPress();
			}
		}

		/// <summary>
		/// Handles a button press
		/// </summary>
		public void HandleButtonPress()
		{
			if (buttonPressed != null)
			{
				buttonPressed.Invoke();
			}
		}
	}
}