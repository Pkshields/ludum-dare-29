﻿using UnityEngine;
using System.Collections;

public static class Tags {
    public const string PLAYER = "Player";
    public const string ENEMY = "Enemy";
    public const string ENEMYBULLET = "EnemyBullet";
    public const string PLAYERBULLET = "PlayerBullet";
    public const string ENEMYCONTAINER = "EnemyContainer";
	public const string GROUND = "Ground";
}
