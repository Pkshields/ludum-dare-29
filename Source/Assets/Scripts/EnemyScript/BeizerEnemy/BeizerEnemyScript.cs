using UnityEngine;
using System.Collections;

public class BeizerEnemyScript : MonoBehaviour {
	public int millisecondsBetweenEmit;
	public int numPerEmit;

    public float maxDistanceFromCenter;

    public GameObject beizerBullet;

	private float timeSinceEmit;

	private void Start () {
		timeSinceEmit = 0;
	}

	private void Update () {
        timeSinceEmit += Time.deltaTime * 1000;

		if (timeSinceEmit >= millisecondsBetweenEmit) {
            for (int i = 0; i < numPerEmit; i++) {
                float distanceFromCenter = Random.Range(-maxDistanceFromCenter, maxDistanceFromCenter);

                GameObject bulletObj = Instantiate(beizerBullet, this.transform.position, Quaternion.identity) as GameObject;
                BeizerBulletMovementScript bullet = bulletObj.GetComponent<BeizerBulletMovementScript>();
                bullet.SetControlPointOffset(distanceFromCenter);

                timeSinceEmit = 0;
            }
		}
	}
}
