﻿using UnityEngine;
using System.Collections;
using UnityBase.Math;

public class PlayerMeleeScript : MonoBehaviour {

	private void Start () {
	
	}
	
	private void Update () {
        if (Input.GetButton(Inputs.FIRE)) {
            Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mouseWorldPosition.z = this.transform.position.z;
            
            Vector3 playerToCamera = mouseWorldPosition - this.transform.position;
            playerToCamera.Normalize();
            float resultingAngleToPointToMouse = MathHelper.VectorToAngle(playerToCamera);
            resultingAngleToPointToMouse = MathHelper.RadiansToDegrees(resultingAngleToPointToMouse);

            //Debug.Log(resultingAngleToPointToMouse);
            if (resultingAngleToPointToMouse > -60f && resultingAngleToPointToMouse <= 60) {
                this.transform.rotation = MathHelper.AngleToQuaternion(MathHelper.DegreesToRadians(0f));
            }
            else if (resultingAngleToPointToMouse > 60f && resultingAngleToPointToMouse <= 180f) {
                this.transform.rotation = MathHelper.AngleToQuaternion(MathHelper.DegreesToRadians(-120f));
            }
            else if (resultingAngleToPointToMouse > -180f && resultingAngleToPointToMouse <= -60f) {
                this.transform.rotation = MathHelper.AngleToQuaternion(MathHelper.DegreesToRadians(-240f));
            }
            else {
                Debug.LogError("ERROR: Unaccounted for rotation on MeleePlayer");
            }
        }
	}
}
