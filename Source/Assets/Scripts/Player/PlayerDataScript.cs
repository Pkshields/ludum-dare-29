﻿using UnityEngine;
using System.Collections;
using UnityBase;

public class PlayerDataScript : MonoBehaviour {
    public int startingHealth;
    public int healPerFrame;
    public GameObject[] playerObjects;

    private float health;
    private int score;

    private HealthBGScript healthBG;
    private GameObject currentPlayer;
    private int currentPlayerNum;

	private void Start() {
        GameObject[] players = GameObject.FindGameObjectsWithTag(Tags.PLAYER);
        if (players.Length == 0) {
            throw new MissingComponentException("Could not find the Player object");
        }

        health = startingHealth;
        score = 0;

        currentPlayer = players[0];
        healthBG = FindObjectOfType<HealthBGScript>();
        currentPlayerNum = 0;
	}
	
	private void Update() {
        HealthCheck();
        CheckPlayerTypeSwap();
        HealOverTime();
	}

    private void SwapPlayer(int playerNum) {
        GameObject newPlayer = Instantiate(playerObjects[playerNum], 
                                           currentPlayer.transform.position, 
                                           currentPlayer.transform.rotation) as GameObject;
        Destroy(currentPlayer);
        currentPlayer = newPlayer;
    }

    private void HealthCheck() {
        if (health <= 0 && !UnityHelper.IsGamePaused()) {
            Application.LoadLevelAdditive("GameOver");
            SoundController.Instance.PlaySoundEffect("Death01");
            UnityHelper.PauseGame(true);
        }
    }

    private void CheckPlayerTypeSwap() {
        if (Input.GetButtonDown(Inputs.ATTACKTYPE)) {
            if (Input.GetAxis(Inputs.ATTACKTYPE) > 0) {
                currentPlayerNum++;
                if (currentPlayerNum >= playerObjects.Length) currentPlayerNum = 0;
                SwapPlayer(currentPlayerNum);
            }
            else if (Input.GetAxis(Inputs.ATTACKTYPE) < 0) {
                currentPlayerNum--;
                if (currentPlayerNum < 0) currentPlayerNum = playerObjects.Length - 1;
                SwapPlayer(currentPlayerNum);
            }
        }
    }

    private void HealOverTime() {
        if (health < startingHealth && !UnityHelper.IsGamePaused()) {
            health += healPerFrame;
            healthBG.SetOpacity(1f - (health / (float)startingHealth));
        }
    }

    public void DoDamage(int damage) {
        health -= damage;
        healthBG.SetOpacity(1f - (health / (float)startingHealth));
    }

    public void AddScore(int score) {
        this.score += score;
    }

    public int GetScore() {
        return score;
    }
}
