﻿using UnityEngine;
using System.Collections;

public class PlayerMovementScript : MonoBehaviour {
	public float speed;

	private void Start() {
		
	}
	
	private void FixedUpdate() {
		float xAxis = Input.GetAxis(Inputs.HORIZONTALMOVEMENT);
		float yAxis = Input.GetAxis(Inputs.VERTICALMOVEMENT);

		Vector3 velocity = new Vector3(
			xAxis * speed, 
			yAxis * speed,
			0);

		this.rigidbody2D.velocity = velocity;
	}
}
