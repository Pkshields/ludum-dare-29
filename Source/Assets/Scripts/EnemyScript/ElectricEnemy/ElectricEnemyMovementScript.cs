using UnityEngine;
using System.Collections;
using UnityBase.Math;

public class ElectricEnemyMovementScript : MonoBehaviour {
    public float maxSpeed;
    public float maxCircleSpeed;
    public float distanceFromPlayer;

    private GameObject player;

	private void Start () {
        GameObject[] players = GameObject.FindGameObjectsWithTag(Tags.PLAYER);
        if (players.Length == 0) {
            throw new MissingComponentException("Could not find the Player object");
        }
        player = players[0];
	}
	
	private void Update () {
        Vector3 playerPosition = player.transform.position;
        Vector2 vectorBetween = playerPosition - this.transform.position;
        float distBetween = vectorBetween.magnitude;

        if (distBetween > distanceFromPlayer) {
            MoveTowardsPlayer();
        }
        else {
            CircleAroundPlayer();
        }
	}

    private void MoveTowardsPlayer() {
        Vector2 playerPosition = player.transform.position;
        Vector2 normalVectorBetween = VectorHelper.CalculateDirectionalVector(this.transform.position, playerPosition);
        
        Vector2 newVelocity = this.rigidbody2D.velocity + normalVectorBetween;
        
        float velocityLength = newVelocity.magnitude;
        if (velocityLength > maxSpeed) {
            float speedCorrection = maxSpeed / velocityLength;
            newVelocity *= speedCorrection;
        }

        this.rigidbody2D.velocity = newVelocity;
    }

    private void CircleAroundPlayer() {
        Vector2 playerPosition = player.transform.position;
        Vector2 normalVectorBetween = VectorHelper.CalculateDirectionalVector(this.transform.position, playerPosition);
        Vector2 perpendicularVector = VectorHelper.CalculatePerpendicularVector(normalVectorBetween);

        Vector2 newVelocity = this.rigidbody2D.velocity + perpendicularVector;
        
        float velocityLength = newVelocity.magnitude;
        if (velocityLength > maxCircleSpeed) {
            float speedCorrection = maxCircleSpeed / velocityLength;
            newVelocity *= speedCorrection;
        }
        
        this.rigidbody2D.velocity = newVelocity;
    }
}
