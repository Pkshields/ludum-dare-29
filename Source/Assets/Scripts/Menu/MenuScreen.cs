﻿using UnityEngine;
using System.Collections;
using UnityBase;

public class MenuScreen : MonoBehaviour {
    public int screenWidth;
    public int screenHeight;
    public GUISkin skin;

	private void Start() {
        SoundController.Instance.InitializeSound("Menu01", true);
        SoundController.Instance.PlaySong("Menu01");
	}
	
	private void OnGUI() {
        float headerWidth = 580f;
        float headerHeight = 50f;

        GUIHelper.ScaleGUIToScreenSize(screenWidth, screenHeight);
        
        GUI.skin = skin;
        GUI.Label(new Rect((screenWidth / 2f) - (headerWidth / 2f), 80f, headerWidth, headerHeight), "The Pixel Underground");

        GUIHelper.ResetGUIScreenSize();
	}
}
