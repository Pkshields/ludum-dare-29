﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour {
    public int health;
    public int pointsWorth;

	private void Start () {
	
	}
	
    private void Update () {
	
	}

    private void OnTriggerEnter2D(Collider2D other) {
        HandleCollision(other);
    }
    
    private void OnTriggerStay2D(Collider2D other) {
        HandleCollision(other);
    }

    private void HandleCollision(Collider2D other) {
        if (other.tag == Tags.PLAYERBULLET) {
            PlayerBulletScript bullet = other.GetComponent<PlayerBulletScript>();
            int damageToDo = bullet.GetDamage();
            health -= damageToDo;

            if (health <= 0) {
                PlayerDataScript playerData = FindObjectOfType<PlayerDataScript>();
                playerData.AddScore(pointsWorth);

                Destroy(this.gameObject);
            }
        }
    }
}
