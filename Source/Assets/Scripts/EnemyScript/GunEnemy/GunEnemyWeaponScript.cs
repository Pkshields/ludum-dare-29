﻿using UnityEngine;
using System.Collections;

public class GunEnemyWeaponScript : MonoBehaviour {
    public int millisecondsBetweenEmit;
    public GameObject gunBullet;

    private float timeSinceEmit;

	private void Start () {
        timeSinceEmit = 0;
	}
	
	private void Update () {
        timeSinceEmit += Time.deltaTime * 1000;
        
        if (timeSinceEmit >= millisecondsBetweenEmit) {
            GameObject bulletObj = Instantiate(gunBullet, this.transform.position, this.transform.rotation) as GameObject;
            timeSinceEmit = 0;
        }
	}
}
