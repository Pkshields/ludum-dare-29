﻿using UnityEngine;
using System.Collections;
using UnityBase;

public class GameOverScript : MonoBehaviour {
    public int screenWidth;
    public int screenHeight;
    public GUISkin skin;

    private int score;

    private void Start() {
        PlayerDataScript playerData = FindObjectOfType<PlayerDataScript>();
        score = playerData.GetScore();
    }
    
    private void Update() {

    }

    private void OnGUI() {
        float headerHeight = 50f;
        float headerWidth = 350f;
        float finalScoreWidth = 360f;
        float scoreWidth = 360f;
        float retryWidth = 850f;
        
        GUIHelper.ScaleGUIToScreenSize(screenWidth, screenHeight);
        
        GUI.skin = skin;
        GUI.Label(new Rect((screenWidth / 2f) - (headerWidth / 2f), 80f, headerWidth, headerHeight), "Game Over");
        GUI.Label(new Rect((screenWidth / 2f) - (finalScoreWidth / 2f), 260f, finalScoreWidth, headerHeight), "Final score:");
        GUI.Label(new Rect((screenWidth / 2f) - (scoreWidth / 2f), 350f, scoreWidth, headerHeight), ""+score);
        GUI.Label(new Rect((screenWidth / 2f) - (retryWidth / 2f), 550f, retryWidth, headerHeight), "Press any button to retry");
        
        GUIHelper.ResetGUIScreenSize();


        if (Event.current.type == EventType.KeyDown) {
            Application.LoadLevel("Game");
        }
    }
}
