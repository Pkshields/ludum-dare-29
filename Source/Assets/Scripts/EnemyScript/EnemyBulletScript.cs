﻿using UnityEngine;
using System.Collections;

public class EnemyBulletScript : MonoBehaviour {
    public int damageToDo;
    public bool destroySelf;

	public void Start () {
	
	}
	
	public void Update () {
	
	}

    public int GetDamage() {
        return damageToDo;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        HandleCollision(other);
    }

    private void OnTriggerStay2D(Collider2D other) {
        HandleCollision(other);
    }

    private void HandleCollision(Collider2D other) {
        if (other.tag == Tags.ENEMY || other.tag == Tags.ENEMYBULLET || other.tag == Tags.PLAYERBULLET) {
            //Don't do any damage to enemy
        }
        else {
            if (destroySelf) {
                Destroy(this.gameObject);
            }
        }
    }
}
