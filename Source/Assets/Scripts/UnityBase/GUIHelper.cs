﻿using UnityEngine;

namespace UnityBase
{
	/// <summary>
	/// Helper methods related to GUIs in Unity
	/// </summary>
	public static class GUIHelper
	{
		/// <summary>
		/// Scales the size of the GUI to a default screen size
		/// </summary>
		/// <param name="screenWidth">Default screen width</param>
		/// <param name="screenHeight">Default screen height</param>
		public static void ScaleGUIToScreenSize(int screenWidth, int screenHeight)
		{
			Vector3 ratio = new Vector3((float)Screen.width / (float)screenWidth, 
                                        (float)Screen.height / (float)screenHeight, 1f);
			Matrix4x4 guiMatrix = Matrix4x4.identity;
            guiMatrix.SetTRS(Vector3.one, Quaternion.identity, ratio);
			GUI.matrix = guiMatrix;
		}

		/// <summary>
		/// Resets the size of the GUI screen size
		/// </summary>
		public static void ResetGUIScreenSize()
		{
			GUI.matrix = Matrix4x4.identity;
		}
	}
}