using UnityEngine;
using System.Collections;

public class TitleScreenManScript : MonoBehaviour {
    public float speed;
    public float jumpHeight;

    private Animator animator;

	private void Start() {
        animator = this.GetComponentInChildren<Animator>();
        this.rigidbody2D.fixedAngle = true;
	}
	
	private void Update() {
        bool xButton = Input.GetButton(Inputs.HORIZONTALMOVEMENT);
        bool xButtonUp = Input.GetButtonUp(Inputs.HORIZONTALMOVEMENT);
        
        if (xButton) {
            float xAxis = Input.GetAxis(Inputs.HORIZONTALMOVEMENT);

            if (xAxis > 0f) {
                animator.SetBool("isWalking", true);
                FlipSprite(false);
                this.transform.position += new Vector3(speed, 0f, 0f);
            }
            else if (xAxis < 0f) {
                animator.SetBool("isWalking", true);
                FlipSprite(true);
                this.transform.position += new Vector3(-speed, 0f, 0f);
            }
        }
        else if (xButtonUp) {
            animator.SetBool("isWalking", false);
        }

        float yAxis = Input.GetAxis(Inputs.VERTICALMOVEMENT);
        bool yButton = Input.GetButtonDown(Inputs.VERTICALMOVEMENT);
        
        if (yButton && yAxis > 0f) {
            this.rigidbody2D.velocity = new Vector2(this.rigidbody2D.velocity.x, jumpHeight);
        }
	}
    
    private void FlipSprite(bool isFlipped) {
        Vector3 scale = this.transform.localScale;
        scale.x = (isFlipped ? Mathf.Abs(scale.x) * -1f : Mathf.Abs(scale.x) * 1f);
        this.transform.localScale = scale;
    }
}
