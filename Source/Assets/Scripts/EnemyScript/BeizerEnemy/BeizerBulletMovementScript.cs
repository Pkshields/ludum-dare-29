﻿using UnityEngine;
using System.Collections;
using UnityBase.Math;

public class BeizerBulletMovementScript : MonoBehaviour {
	public float timeToTravel;

	private Vector2 startPoint;
	private Vector2 endPoint;
	private Vector2 controlPoint;
	private float bezierTime;

	private void Awake() {
		GameObject[] players = GameObject.FindGameObjectsWithTag(Tags.PLAYER);
		if (players.Length == 0) {
			throw new MissingComponentException("Could not find the Player object");
		}
		GameObject player = players[0];

		startPoint = this.transform.position;
		endPoint = player.transform.position;
		bezierTime = 0f;

		controlPoint = VectorHelper.CalculateMidpoint(startPoint, endPoint);
	}
	
	private void Update() {
		//Bezier curve algorithm! Adapted from http://answers.unity3d.com/questions/12689/moving-an-object-along-a-bezier-curve.html

		bezierTime = bezierTime + (Time.deltaTime / timeToTravel);
		
		if (bezierTime >= 1) {
			Destroy(this.gameObject);
		}

		float curveX = (((1-bezierTime)*(1-bezierTime)) * startPoint.x) + (2 * bezierTime * (1 - bezierTime) * controlPoint.x) + ((bezierTime * bezierTime) * endPoint.x);
		float curveY = (((1-bezierTime)*(1-bezierTime)) * startPoint.y) + (2 * bezierTime * (1 - bezierTime) * controlPoint.y) + ((bezierTime * bezierTime) * endPoint.y);
		this.transform.position = new Vector3(curveX, curveY, this.transform.position.z);
	}

	public void SetControlPointOffset(float offset) {
		Vector2 midpoint = VectorHelper.CalculateMidpoint(startPoint, endPoint);
		Vector2 directionalVector = VectorHelper.CalculateDirectionalVector(startPoint, endPoint);
		Vector2 perpendicularVector = VectorHelper.CalculatePerpendicularVector(directionalVector);

		controlPoint = midpoint + (perpendicularVector * offset);
	}
}
