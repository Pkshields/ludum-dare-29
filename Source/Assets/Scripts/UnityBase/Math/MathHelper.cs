using UnityEngine;

namespace UnityBase.Math
{
	/// <summary>
	/// Helper class to help with some math calculation
	/// </summary>
	public static class MathHelper
	{
		/// <summary>
		/// Convert a 2D vector into an angle clockwise
		/// </summary>
		/// <returns>Resulting angle in radians</returns>
		/// <param name="vector">Vector to convert</param>
		public static float VectorToAngle(Vector2 vector)
		{
			float result;
			if (vector.x >= 0)
			{
				result = DegreesToRadians(Vector2.Angle(Vector2.up, vector));
			}
			else
			{
				result = DegreesToRadians(Vector2.Angle(Vector2.up, vector)) * -1;
			}

			return result;
		}

		/// <summary>
		/// Converts a clockwise rotation into a 2D directional vector
		/// </summary>
		/// <returns>Resulting vector</returns>
		/// <param name="angle">Angle in radians to convert</param>
		public static Vector2 AngleToVector(float angle)
		{
			float y = Mathf.Cos(angle);	
			float x = Mathf.Sin(angle);
			return new Vector2(x, y);
		}

		public static Quaternion AngleToQuaternion(float angle)
		{
			//return new Quaternion(0, 0, Mathf.Sin(angle / 2), Mathf.Cos(angle / 2));
			return Quaternion.AngleAxis(RadiansToDegrees(angle), Vector3.forward);
		}

		/// <summary>
		/// Get the 2D rotation (anticlockwise) for a GameObject
		/// </summary>
		/// <returns>The 2D rotation</returns>
		/// <param name="obj">GameObject</param>
		public static float Get2DObjectRotation(GameObject obj)
		{
			return obj.transform.rotation.eulerAngles.z;
		}

		/// <summary>
		/// Converts degrees to radians
		/// </summary>
		/// <returns>Radians</returns>
		/// <param name="deg">Degrees</param>
		public static float DegreesToRadians(float deg) {
			return deg * Mathf.Deg2Rad;
		}

		/// <summary>
		/// Converts radians to degrees
		/// </summary>
		/// <returns>Degrees</returns>
		/// <param name="rad">Radians</param>
		public static float RadiansToDegrees(float rad) {
			return rad * Mathf.Rad2Deg;
		}
	}
}