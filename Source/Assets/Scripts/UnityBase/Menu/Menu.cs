﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace UnityBase.Menu
{
	/// <summary>
	/// Menu container to manage all the buttons
	/// </summary>
	public class Menu
	{
		/// <summary>
		/// List of buttons
		/// </summary>
		private List<MenuButton> buttons;

		/// <summary>
		/// Are we using this menu in Gamepad Mode?
		/// </summary>
		private bool gamepadMode;

		/// <summary>
		/// Current buttong hovered over by gamepad
		/// </summary>
		private int currentGamepadButton;

		/// <summary>
		/// GUISkin	to use
		/// </summary>
		private GUISkin skin;

		/// <summary>
		/// Initializes a new instance of the <see cref="UnityBase.Menu.Menu"/> class
		/// </summary>
		/// <param name="gamepadMode">If set to <c>true</c> if we are using gamepad mode</param>
		public Menu(bool gamepadMode) 
		{
			buttons = new List<MenuButton>();
			this.gamepadMode = gamepadMode;
			currentGamepadButton = -1;
		}

		/// <summary>
		/// Draws the menu and handle mouse clicks
		/// </summary>
		public void DrawAndHandle()
		{
			if (skin != null)
			{
				GUI.skin = skin;
			}

			for (int i = 0; i < buttons.Count; i++)
			{
				buttons[i].DrawAndHandle();
			}

			if (gamepadMode)
			{
				GUI.FocusControl(buttons[currentGamepadButton].controlName);
			}
		}

		/// <summary>
		/// Moves the menu down in Gamepad mode
		/// </summary>
		public void MoveDown()
		{
			currentGamepadButton++;
			if (currentGamepadButton >= buttons.Count)
			{
				currentGamepadButton = 0;
			}
		}

		/// <summary>
		/// Moves the menu up in Gamepad mode
		/// </summary>
		public void MoveUp()
		{
			currentGamepadButton--;
			if (currentGamepadButton < 0)
			{
				currentGamepadButton = buttons.Count - 1;
			}
		}

		/// <summary>
		/// Press the current button hovered over by the gamepad
		/// </summary>
		public void PressButton()
		{
			buttons[currentGamepadButton].HandleButtonPress();
		}

		/// <summary>
		/// Add a button to the menu
		/// </summary>
		/// <param name="button">Button to add</param>
		public void AddButton(MenuButton button)
		{
			buttons.Add(button);

			if (gamepadMode && buttons.Count == 1)
			{
				GUI.FocusControl(button.controlName);
				currentGamepadButton = 0;
			}
		}

		/// <summary>
		/// Set the GUISkin to draw with
		/// </summary>
		/// <param name="skin">Skin to use</param>
		public void SetGUISkin(GUISkin skin)
		{
			this.skin = skin;
		}
	}
}