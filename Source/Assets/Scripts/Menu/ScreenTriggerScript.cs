﻿using UnityEngine;
using System.Collections;

public class ScreenTriggerScript : MonoBehaviour {
    private string screenToTrigger;

    private void Start() {

    }
    
    private void Update() {
        
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.tag == Tags.PLAYER) {
            Application.LoadLevel(screenToTrigger);
        }
    }

    public void SetScreenToTrigger(string screenToTrigger) {
        this.screenToTrigger = screenToTrigger;
    }
}
