﻿using UnityEngine;
using System.Collections;

public class GunEnemyMovementScript : MonoBehaviour {
    public float rotationSpeed;

	private void Start () {
	
	}
	
	private void Update () {
        Vector3 rotation = this.transform.eulerAngles;
        rotation.z += rotationSpeed;
        this.transform.eulerAngles = rotation;

        if (this.GetComponentsInChildren<EnemyScript>().Length == 0) {
            Destroy(this.gameObject);
        }
	}
}
