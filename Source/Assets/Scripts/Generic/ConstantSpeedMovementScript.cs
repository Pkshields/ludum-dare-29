﻿using UnityEngine;
using System.Collections;
using UnityBase.Math;

public class ConstantSpeedMovementScript : MonoBehaviour {
	public float speed;
	
	private void Start() {
		float objRotation = MathHelper.Get2DObjectRotation(this.gameObject) * -1;	// * -1? Converts Unity's anticlockwise rotation 
																					// into a clockwise one for our calculations
		objRotation = MathHelper.DegreesToRadians(objRotation);
		Vector2 direction = MathHelper.AngleToVector(objRotation);

		this.rigidbody2D.velocity = direction * speed;
		this.rigidbody2D.drag = 0f;
		this.rigidbody2D.angularDrag = 0f;
	}
	
	private void Update() {
		
	}
}
