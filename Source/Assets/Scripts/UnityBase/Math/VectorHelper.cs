using UnityEngine;

namespace UnityBase.Math
{
    /// <summary>
    /// Helper class to help with some vector math
    /// </summary>
    public static class VectorHelper
    {
        /// <summary>
        /// Calculates the midpoint between two vectors
        /// </summary>
        /// <returns>The midpoint between the two vectors</returns>
        /// <param name="start">Start vector</param>
        /// <param name="end">End vector</param>
        public static Vector2 CalculateMidpoint(Vector2 start, Vector2 end)
        {
            return new Vector2((start.x + end.x) / 2, (start.y + end.y) / 2);
        }

        /// <summary>
        /// Calculates the directional vector between two vectors
        /// </summary>
        /// <returns>The directional vector</returns>
        /// <param name="start">Start vector</param>
        /// <param name="end">End vector</param>
        public static Vector2 CalculateDirectionalVector(Vector2 start, Vector2 end)
        {
            Vector2 result = end - start;
            result.Normalize();
            return result;
        }

        /// <summary>
        /// Calculates the perpendicular vector (90 degress from provided vector)
        /// </summary>
        /// <returns>The perpendicular vector</returns>
        /// <param name="vector">Vector to perpendicular</param>
        public static Vector2 CalculatePerpendicularVector(Vector2 vector)
        {
            return new Vector2(-vector.y, vector.x);
        }
    }
}