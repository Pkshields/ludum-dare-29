﻿using UnityEngine;
using System.Collections;

public class ButtonScript : MonoBehaviour {
    public string screenToTrigger;

    private ScreenTriggerScript screenTrigger;

	private void Start() {
        screenTrigger = FindObjectOfType<ScreenTriggerScript>();
	}
	
    private void Update() {
	    
	}

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.tag == Tags.PLAYER) {
            screenTrigger.SetScreenToTrigger(screenToTrigger);
            GameObject ground = GameObject.FindGameObjectWithTag(Tags.GROUND);
            Destroy(ground);
        }
    }
}
