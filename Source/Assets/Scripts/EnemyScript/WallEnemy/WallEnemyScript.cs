﻿using UnityEngine;
using System.Collections;

public class WallEnemyScript : MonoBehaviour {
    public Axis axis;

    private GameObject player;

	private void Start () {
        GameObject[] players = GameObject.FindGameObjectsWithTag(Tags.PLAYER);
        if (players.Length == 0) {
            throw new MissingComponentException("Could not find the Player object");
        }
        player = players[0];
	}
	
	private void Update () {
        Vector2 playerPosition = player.transform.position;
        Vector3 currentPosition = this.transform.position;

        if (axis == Axis.X) {
            currentPosition.x = playerPosition.x;
        }
        else if (axis == Axis.Y) {
            currentPosition.y = playerPosition.y;
        }

        this.transform.position = currentPosition;
	}
}

public enum Axis {
    X,
    Y
}