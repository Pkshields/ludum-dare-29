﻿using UnityEngine;
using System.Collections;
using UnityBase.Math;
using UnityBase;

public class PlayerWeaponScript : MonoBehaviour {
	public Rigidbody2D ammo;
	public int timeBetweenGunfire;

	private long lastFireTime;

	private void Start() {
		lastFireTime = UnityHelper.GetTotalTimeInMilliseconds();
	}
	
	private void FixedUpdate() {
		if (Input.GetButton(Inputs.FIRE) && 
		    lastFireTime + timeBetweenGunfire < UnityHelper.GetTotalTimeInMilliseconds()) {

			Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			mouseWorldPosition.z = this.transform.position.z;

			Vector3 playerToCamera = mouseWorldPosition - this.transform.position;
			playerToCamera.Normalize();
			float resultingAngleToPointToMouse = MathHelper.VectorToAngle(playerToCamera);

			Instantiate(ammo, this.transform.position, MathHelper.AngleToQuaternion(resultingAngleToPointToMouse * -1));

            SoundController.Instance.PlaySoundEffect("Gun01");

			lastFireTime = UnityHelper.GetTotalTimeInMilliseconds();
		}
	}
}
