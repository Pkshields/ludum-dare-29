﻿using UnityEngine;
using System.Collections;

public static class Inputs {
	public const string HORIZONTALMOVEMENT = "HorizontalMovement";
	public const string VERTICALMOVEMENT = "VerticalMovement";
    public const string FIRE = "Fire";
	public const string ATTACKTYPE = "AttackType";
}
