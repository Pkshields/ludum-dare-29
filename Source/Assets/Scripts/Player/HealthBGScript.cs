﻿using UnityEngine;
using System.Collections;

public class HealthBGScript : MonoBehaviour {
    private SpriteRenderer spriteRenderer;

    private void Start() {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

	public void SetOpacity(float opacity) {
        Color color = spriteRenderer.color;
        color.a = opacity;
        spriteRenderer.color = color;
	}
}
