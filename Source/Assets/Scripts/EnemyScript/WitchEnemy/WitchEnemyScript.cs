﻿using UnityEngine;
using System.Collections;

public class WitchEnemyScript : MonoBehaviour {
    private WitchEnemyMovementScript movementScript;

	private void Start () {
        movementScript = GetComponent<WitchEnemyMovementScript>();
	}
	
	private void Update () {
	    
	}

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == Tags.PLAYERBULLET) {
            if (!movementScript.IsStarted()) {
                movementScript.Startle();
                SoundController.Instance.PlaySoundEffect("Witch01");
            }
        }
    }
}
