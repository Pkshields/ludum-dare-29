﻿using UnityEngine;
using System.Collections;
using UnityBase;

public class LevelManagerScript : MonoBehaviour {
    public int numLevels;
    public string levelPrefix;

    private GameObject enemyContainer;
    private int currentLevel;

	private void Start() {
        currentLevel = -1;
        UnityHelper.PauseGame(false);
        LoadNewLevel();
        SoundController.Instance.PlaySong("Game01");
        SoundController.Instance.PlaySoundEffect("NewLevel01");
	}
	
	private void Update() {
        if (enemyContainer != null && 
            enemyContainer.transform.childCount <= 0 && 
            !UnityHelper.IsGamePaused()) {

            Destroy(enemyContainer);
            LoadNewLevel();
            SoundController.Instance.PlaySoundEffect("NewLevel01");
        }
        else if (enemyContainer == null) {
            try {
                enemyContainer = GameObject.FindGameObjectWithTag(Tags.ENEMYCONTAINER);
            }
            catch {
                Debug.LogError("ERROR: Could not find the EnemyContainer on the level");
            }
        }
	}

    private void LoadNewLevel() {
        int levelToLoad;
        do {
            levelToLoad = Random.Range(0, numLevels - 1);
        }
        while (currentLevel == levelToLoad);
        
        Application.LoadLevelAdditive(levelPrefix + levelToLoad);
        enemyContainer = GameObject.FindGameObjectWithTag(Tags.ENEMYCONTAINER);
        currentLevel = levelToLoad;
    }
}
